// ==UserScript==
// @name			Lemmy Universal Link Switcher
// @namespace		http://azzurite.tv/
// @license			GPLv3
// @version			1.3.4
// @description		Ensures that all URLs to Lemmy instances always point to your main/home instance.
// @homepageURL		https://gitlab.com/azzurite/lemmy-universal-link-switcher
// @supportURL		https://gitlab.com/azzurite/lemmy-universal-link-switcher/-/issues
// @updateURL		https://gitlab.com/azzurite/lemmy-universal-link-switcher/-/jobs/artifacts/main/raw/dist/lemmy-universal-link-switcher.user.js?job=build
// @author			Azzurite
// @match			*://*/*
// @icon			https://gitlab.com/azzurite/lemmy-universal-link-switcher/-/raw/main/resources/favicon.png?inline=true
// @grant			GM.setValue
// @grant			GM.getValue
// @grant			GM.xmlHttpRequest
// @grant			GM.registerMenuCommand
// @connect			*
// @require			https://unpkg.com/@popperjs/core@2
// @require			https://unpkg.com/tippy.js@6
// ==/UserScript==
