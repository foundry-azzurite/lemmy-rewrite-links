import * as esbuild from "esbuild";
import * as fs from "node:fs";
import * as path from "path";
import { fileURLToPath } from "url";
import jasmineBrowser from "jasmine-browser-runner";
import selenium from "selenium-webdriver";
import debounce from "debounce";
import JasmineConsoleReporter from "jasmine-console-reporter";

const logBuild = {
	name: `logBuild`,
	setup(build) {
		build.onStart(() => {
			console.log(new Date(), `Build start`);
		});
		build.onEnd(result => {
			if (result.errors.length) return;
			console.log(new Date(), `Build success`)
		})
	},
}

let config = {
	entryPoints: [`src/main.js`],
	bundle: true,
	outfile: `dist/lemmy-universal-link-switcher.user.js`,
	banner: {
		js: fs.readFileSync(`resources/header.js`, `utf-8`)
	},
	plugins: [logBuild]
};

function copyDevScript() {
	const output = fs.readFileSync(`dist/lemmy-universal-link-switcher.user.js`, `utf-8`);
	if (!fs.existsSync(`dev`)) {
		fs.mkdirSync(`dev`);
	}
	fs.writeFileSync(`dev/dev-script.user.js`, output.replace(`var DEBUG = false;`, `var DEBUG = "trace";`));
}

const mode = process.argv[3];
if (mode === `build`) {
	await esbuild.build(config);
} else if (mode === `watch`) {
	const context = await esbuild.context(config);

	const projectBaseDir = path.resolve(fileURLToPath(import.meta.url), `../..`);
	const jasmineConf = {
		projectBaseDir,
		srcDir: `src`,
		specDir: `src`,
		specFiles: ["**/*.test.js"],
		esmFilenameExtension: `.js`,
	}

	const port = 8888;
	jasmineBrowser.startServer(jasmineConf, {port})

	const driver = new selenium.Builder().forBrowser(`firefox`).build();
	const runner = new jasmineBrowser.Runner({
		webdriver: driver,
		reporters: [
			{specDone(spec) {
				spec.failedExpectations.forEach(exp => {
					if (exp.stack) {
						// nicer formatting
						exp.stack = exp.stack
							.replace(`<Jasmine>\n`, ``)
							.replace(`\n<Jasmine>`, ``)
							.replaceAll(`@http://localhost:${port}/__spec__`, `\t at src`);
					}
				})
			}}, 
			new JasmineConsoleReporter({verbosity: 0})
		],
		host: `http://localhost:${port}`
	});

	async function rebuild() {
		try {
			await context.rebuild();
			copyDevScript();
			await runner.run();
		} catch (e) {
			// esbuild reports errors itself
		}
	}

	rebuild();
	const debouncedRebuild = debounce(rebuild, 200); 
	fs.watch(`src`, { recursive: true }, debouncedRebuild);
	fs.watch(`resources`, { recursive: true }, debouncedRebuild);
}

