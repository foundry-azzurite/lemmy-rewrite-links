# 1.3.4

* Updated instance list

# 1.3.3

* Fix uncaught exception for relative links

# 1.3.2

* Updated instance list

# 1.3.1

* Fix links opening in both main & new tab when holding the meta key on Mac OS
* Updated instance list

# 1.3.0

* Added settings menu
* Added manual entry of home instance, allowing to add an instance not on the list as a home
* Added secondary home instances, links pointing to those will not get rewritten (links to all other instances still get rewritten to your main home instance)
* Updated instance list

# 1.2.4

* Fix styles breaking on some sites

# 1.2.3

* Updated instance list

# 1.2.2

* Fix tooltip not being openable from mobile, always taking you to the link target
* Fix multiple instances of the "Show at home" button appearing

# 1.2.1

* Add `@connect *` tag to enable "Always allow all domains" button on Tampermonkey

# 1.2.0

* Add rewrite support for posts/comments
* Try to not rewrite federation links

# 1.1.2

* Fix kbin user URLs with "@" at the beginning

# 1.1.1

* Fix "Show at home" link in header not showing up on Lemmy 0.17.x sites
* Fix links breaking on some other pages
* Fix explicit federation links (from home instance to source instance) being rewritten

# 1.1.0

* Fix occasional double link icons
* Fix for lemmy 0.18
* Increase performance
* Add basic kbin support (also only magazines/users for now)

# 1.0.2

* Fix home being retrieved too late

# 1.0.1

* Migrate from `GM_` methods to `GM.`
* Show "Make Home" button on settings page if no home was set yet
* Pick up home changes immediately
