### Description

Lemmy Universal Link Switcher, or LULs for short, scans all links on all websites, and if any link points to a Lemmy instance that is not your main/home instance, it rewrites the link so that it instead points to your main instance.

Also works on Firefox Android with the Tampermonkey extension!

### Features

* Rewrite links to Lemmy posts/comments to point to your home instance. Only after hovering over them, because getting home posts/comments links require communicating with the Lemmy servers, and we don't want to spam the servers.  
  ![video demonstrating links to posts being rewritten](https://images.azzurite.tv/uploads/original/66/12/bfd69d133cf48bdf438b23638150.mp4)
* Instantly rewrite all links of community or user links to Lemmy/kbin on all websites everywhere to your new instance! The rewritten links will have an icon next to it, and hovering/touching the icon will show you the original link, allowing you to go there if you want to.   
  [![community links being rewritten](https://greasyfork.org/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBNHUxQVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--ab4ca68d48f3174ff9fe6cfd7820c66fa4c08ee9/2023-06-23%2006_00_01.png?locale=en)](https://greasyfork.org/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBNHUxQVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--ab4ca68d48f3174ff9fe6cfd7820c66fa4c08ee9/2023-06-23%2006_00_01.png?locale=en)

* If you are already on a page that has a corresponding page on your home instance, a link will automatically be added to the page header.  
  [![the header link to your home instance when already on a remote post](https://greasyfork.org/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBeGEyQVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--e824ea55c69b88fb392b9ab0cfd093746ad255a0/2023-06-25%2004_58_33.webp?locale=en)](https://greasyfork.org/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBeGEyQVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--e824ea55c69b88fb392b9ab0cfd093746ad255a0/2023-06-25%2004_58_33.webp?locale=en)


### Home Instance Setup

Simply visit the Lemmy instance you want to set as your home while the script is active. You will be asked if you want to set this instance to your home instance:  
[![popup to select home instance](https://greasyfork.org/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBMSs5QVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--8c7af9537ae007ae0032d314d8f03081132c69dc/2023-07-20%2008_39_14-firefox.png?locale=en)](https://greasyfork.org/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBMSs5QVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--8c7af9537ae007ae0032d314d8f03081132c69dc/2023-07-20%2008_39_14-firefox.png?locale=en)

### Settings

If you want to change your home instance or add secondary instances in case you have multiple accounts, simply go to the script settings, accessible within Violent/Tampermonkey directly, or alternatively within any Lemmy instances' settings.  
[![button to set your new home instance](https://greasyfork.org/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBMTI5QVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--a0411f36cc2e5e0c8212fb1582b8dc4ed6f3a9a8/2023-07-20%2008_32_11-ShareX.webp?locale=en)](https://greasyfork.org/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBMTI5QVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--a0411f36cc2e5e0c8212fb1582b8dc4ed6f3a9a8/2023-07-20%2008_32_11-ShareX.webp?locale=en)

### Common Issues

* Does **not** work with **Greasemonkey**. Use [Violentmonkey (open source)](https://violentmonkey.github.io/get-it/) or Tampermonkey (closed source) instead.
* Only proper links (in code: `<a>` tags with a `href` attribute) will be rewritten. If there's a button somewhere with some code attached and that code actually does the linking, I have no way of finding out where that leads, so I can't rewrite it either. So some things that look like links I can't actually rewrite with this script.

### Coming soon

* Rewrite kbin post/comment links
* Better rewriting support for kbin community/user urls (e.g. sort options are currently ignored)
* Nicer tooltip styling (fit into page theme)
* Signify that "Show at home" button is loading for posts/comments
* Integrate with lmmy.to redirector
