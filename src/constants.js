export default {
	ICON_CLASS: withNS(`icon`),
	ICON_LOADING_CLASS: withNS(`loading`),
	ICON_STYLES_ID: withNS(`icon-styles`),
	ICON_LINK_CLASS: withNS(`icon-link`),
	ICON_LINK_SYMBOL_ID: withNS(`icon-link-symbol`),
	ICON_SPINNER_CLASS: withNS(`icon-spinner`),
	ICON_SPINNER_SYMBOL_ID: withNS(`icon-spinner-symbol`),
	ORIGINAL_LINK_CLASS: withNS(`original-link`),
	SHOW_AT_HOME_BUTTON_CLASS: withNS(`show-at-home`),
	ICON_SVG_TEMPLATE_ID: withNS(`icon-template`),
	AUTH_WRONG: `AUTH_WRONG`,
	AUTH_MISSING: `AUTH_MISSING`,
	REWRITE_STATUS: withNSCamelCase(`localUrlStatus`),
	REWRITE_STATUS_PENDING: `pending`,
	REWRITE_STATUS_SUCCESS: `success`,
	REWRITE_STATUS_ERROR: `error`,
	REWRITE_STATUS_UNRESOLVED: `unresolved`,
	SETUP_AUTH_MESSAGE: `Lemmy Universal Link Switcher: Visit your home instance once to set up post/comment rewriting`,
	SETTINGS_BUTTON_ID: withNS(`open-settings-button`),
	SETTINGS_MENU_ID: withNS(`settings`),
	SETTINGS_STYLES_ID: withNS(`settings-styles`),
};

function withNS(identifier) {
	return `lemmy-rewrite-urls-` + identifier;
}
function withNSCamelCase(identifier) {
	return `lemmyRewriteUrls` + identifier.charAt(0).toUpperCase() + identifier.slice(1);
}
