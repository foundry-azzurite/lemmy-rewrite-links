import {findLocalUrl} from './url-mapping.js';
import {setTestHome} from './home.test.js';

const mockHome = `test`;
setTestHome(mockHome)

describe(`local lemmy.world -> local home`, () => {
	it(`should rewrite community url`, () => {
		expect(findLocalUrl(new URL(`https://lemmy.world/c/test`))).toBe(`${mockHome}/c/test@lemmy.world`);
	});
	it(`should rewrite community url with search and hash part`, () => {
		expect(findLocalUrl(new URL(`https://lemmy.world/c/test?dataType=Post&page=1&sort=New#anchor`)))
			.toBe(`${mockHome}/c/test@lemmy.world?dataType=Post&page=1&sort=New#anchor`);
	});
	it(`should rewrite user url`, () => {
		expect(findLocalUrl(new URL(`https://lemmy.world/u/test`))).toBe(`${mockHome}/u/test@lemmy.world`);
	});
});

describe(`remote lemmy.world -> local home`, () => {
	it(`should rewrite community url`, () => {
		expect(findLocalUrl(new URL(`https://lemmy.world/c/test@lemmy.ml`))).toBe(`${mockHome}/c/test@lemmy.ml`);
	});
	it(`should rewrite user url`, () => {
		expect(findLocalUrl(new URL(`https://lemmy.world/u/test@lemmy.ml`))).toBe(`${mockHome}/u/test@lemmy.ml`);
	});
});

describe(`remote kbin.social -> local home`, () => {
	it(`should rewrite community url with sort`, () => {
		expect(findLocalUrl(new URL(`https://kbin.social/m/test/top/%E2%88%9E/links`)))
			.toBe(`${mockHome}/c/test@kbin.social`);
	});
	it(`should not rewrite post urls`, () => {
		expect(findLocalUrl(new URL(`https://kbin.social/m/test/t/12345/title`)))
			.toBeNull();
	});
	it(`should not rewrite microblog urls`, () => {
		expect(findLocalUrl(new URL(`https://kbin.social/m/test/p/12345/title`)))
			.toBeNull();
	});
	it(`should not rewrite microblog overview urls`, () => {
		expect(findLocalUrl(new URL(`https://kbin.social/m/test/microblog`)))
			.toBeNull();
	});
	it(`should not rewrite magazine people urls`, () => {
		expect(findLocalUrl(new URL(`https://kbin.social/m/test/people`)))
			.toBeNull();
	});
});
