import C from '../../constants.js';
import {debug, trace} from '../../debug.js';
import {isHashLink, isSamePage, stopEventHandler} from '../helpers.js';
import {
	fetchLocalUrl,
	findLocalUrl,
	isInstantlyRewritable,
	isRemoteUrl,
	isRewritableAfterResolving
} from '../url-mapping.js';
import {createIcon, getIcon} from './icon.js';
import {
	createLinkLoadTooltip,
	createOriginalLinkTooltip,
	linkLoadTooltipError,
	linkLoadTooltipSuccess
} from './tooltip.js';

export {
	rewriteLinksToLocal
}

function changeLinkHref(link, localUrl) {
	const treeWalker = document.createTreeWalker(link, NodeFilter.SHOW_TEXT, (node) => {
		if (node.textContent.toLowerCase().trim() === link.href.toLowerCase().trim()) {
			return NodeFilter.FILTER_ACCEPT;
		} else {
			return NodeFilter.FILTER_SKIP;
		}
	});
	let textNode;
	while ((textNode = treeWalker.nextNode()) !== null) {
		textNode.textContent = localUrl;
	}
	link.href = localUrl;
	link.addEventListener(`click`, (event) => {
		// Fix for SPAs
		if (event.button === 0 && !event.ctrlKey && !event.metaKey && link.target !== `_blank`) {
			location.href = localUrl;
		}
	});
}

function appendIconTo(elem, icon) {
	if (elem.children.length === 0 || getComputedStyle(elem.children[elem.children.length - 1]).display !== `inline-block`) {
		elem.append(icon);
	} else {
		appendIconTo(elem.children[elem.children.length - 1], icon);
	}
}

function addFetchLocalUrlHandler(link) {
	let tooltip;
	const handler = async (event) => {
		if (event.type === `click`) {
			stopEventHandler(event);
			if (tooltip) tooltip.show();
			return;
		}

		link.removeEventListener(`focus`, handler);
		link.removeEventListener(`mouseenter`, handler);

		if (link.dataset[C.REWRITE_STATUS] === C.REWRITE_STATUS_PENDING) return;
		link.dataset[C.REWRITE_STATUS] = C.REWRITE_STATUS_PENDING;
		
		tooltip = createLinkLoadTooltip(link);

		try {
			const localUrl = await fetchLocalUrl(link);
			if (!localUrl) {
				debug(`Local URL for ${link.href} could not be found`);
				linkLoadTooltipError(tooltip, `Home URL could not be found`);
				return;
			}
			trace(`Local URL for ${link.href} found: ${localUrl}`);
			const oldHref = link.href;
			changeLinkHref(link, localUrl);
			linkLoadTooltipSuccess(tooltip, oldHref);
			link.dataset[C.REWRITE_STATUS] = C.REWRITE_STATUS_SUCCESS;
		} catch (e) {
			debug(`Error while trying to resolve local URL`, e);
			let msg;
			if (e === C.AUTH_WRONG) {
				msg = `Saved login expired. Return to your home instance and log in again.`;
			} else if (e === C.AUTH_MISSING) {
				msg = C.SETUP_AUTH_MESSAGE;
			} else {
				msg = `Error while trying to find home URL`;
			}
			linkLoadTooltipError(tooltip, msg);
			link.dataset[C.REWRITE_STATUS] = C.REWRITE_STATUS_ERROR;
		} finally {
			link.removeEventListener(`click`, handler);
		}
	};
	link.addEventListener(`click`, handler);
	link.addEventListener(`focus`, handler);
	link.addEventListener(`mouseenter`, handler);
}

function isFediverseLink(link) {
	const svg = link.querySelector(`svg`);
	if (!svg) return false;
	if (svg.children.length === 0) return false;
	return svg.children[0].getAttribute(`xlink:href`)?.includes(`#icon-fedilink`);
}

function rewriteToLocal(link) {
	if (!link.parentNode) return false; // DOM got changed under us
	if (link.classList.contains(C.ORIGINAL_LINK_CLASS)) return false;
	if (link.dataset[C.REWRITE_STATUS] === C.REWRITE_STATUS_SUCCESS) return false;
	if (isHashLink(link)) return false;
	if (!isRemoteUrl(link)) return false;
	if (isFediverseLink(link)) return false; 

	if (isInstantlyRewritable(link)) {
		const localUrl = findLocalUrl(link);
		if (!localUrl) return false;

		if (isSamePage(new URL(localUrl), location)) return false; // Probably a federation source link

		const oldHref = link.href;
		changeLinkHref(link, localUrl);

		const icon = createIcon(link);
		appendIconTo(link, icon);
		
		createOriginalLinkTooltip(link, oldHref);

		link.dataset[C.REWRITE_STATUS] = C.REWRITE_STATUS_SUCCESS;
		trace(`Rewrite link`, link, ` from`, oldHref, `to`, localUrl);
		return true;
	} else if (isRewritableAfterResolving(link)) {
		if (!getIcon(link)) {
			appendIconTo(link, createIcon(link));
		}
		if (!link.dataset[C.REWRITE_STATUS]) {
			link.dataset[C.REWRITE_STATUS] = C.REWRITE_STATUS_UNRESOLVED;
			addFetchLocalUrlHandler(link);
		}
	}
}

function findLinksInChange(change) {
	if (change.type === `childList`) {
		const links = Array.from(change.addedNodes)
			.flatMap((addedNode) => {
				if (addedNode.tagName?.toLowerCase() === `a`) {
					return addedNode
				} else if (addedNode.querySelectorAll) {
					return Array.from(addedNode.querySelectorAll(`a`));
				} else {
					return [];
				}
			});
		if (links.length > 0) trace(`Change`, change, `contained the links`, links);
		return links;
	} else if (change.type === `attributes`) {
		return change.target.matches?.(`a`) ? change.target : [];
	} else {
		return [];
	}
}

function findLinksToRewrite(changes) {
	if (!changes) {
		return document.querySelectorAll(`a`);
	}

	return changes.flatMap(findLinksInChange);
}

async function rewriteLinksToLocal(changes) {
	const links = findLinksToRewrite(changes);
	const chunkSize = 50;
	return await (async function processChunk(currentChunk) {
		const startIdx = currentChunk * chunkSize;
		const endChunkIdx = (currentChunk + 1) * chunkSize;
		const endIdx = Math.min(links.length, endChunkIdx);

		debug(`Processing ${links.length} links, current chunk `, currentChunk,
			`processing links ${startIdx} to ${endIdx}`)
		let anyRewritten = false;
		for (let i = startIdx; i < endIdx; ++i) {
			const rewritten = rewriteToLocal(links[i]);
			anyRewritten = anyRewritten || rewritten;
		}
		debug(`Processed links ${startIdx} to ${endIdx}`);

		if (endChunkIdx >= links.length) {
			return anyRewritten;
		}

		const chunkResult = await (new Promise(resolve => setTimeout(async () => {
			resolve(await processChunk(currentChunk + 1));
		}, 0)));
		return anyRewritten || chunkResult;
	})(0);
}
