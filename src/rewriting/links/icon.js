import C from '../../constants.js';
import {registerAddedNode} from '../../our-changes.js';

export {
	createIcon,
	getIcon
}

function getIcon(link) {
	return link.querySelector(`.` + C.ICON_CLASS)
}

function createIcon(link) {
	ensureTemplateAvailable();
	ensureIconStylesAdded();

	const wrapper = document.createElement(`span`);
	registerAddedNode(C.ICON_CLASS, `.` + C.ICON_CLASS);
	wrapper.classList.add(C.ICON_CLASS);
	if (link.children.length === 0 || getComputedStyle(link.children[link.children.length - 1]).marginRight === `0px`) {
		wrapper.style.marginLeft = `0.5em`;
	}
	
	const linkIcon = createSVG();
	linkIcon.classList.add(C.ICON_LINK_CLASS);
	linkIcon.innerHTML = `<use href=#${C.ICON_LINK_SYMBOL_ID} />`;
	wrapper.append(linkIcon);
	
	const spinnerIcon = createSVG();
	spinnerIcon.classList.add(C.ICON_SPINNER_CLASS);
	spinnerIcon.innerHTML = `<use href=#${C.ICON_SPINNER_SYMBOL_ID} />`;
	wrapper.append(spinnerIcon);

	return wrapper;
}


function createSVG() { return document.createElementNS(`http://www.w3.org/2000/svg`, `svg`) }

function ensureTemplateAvailable() {
	if (document.querySelector(`#` + C.ICON_SVG_TEMPLATE_ID)) return;

	const template = createSVG();
	template.id = C.ICON_SVG_TEMPLATE_ID
	template.innerHTML =
		`<defs>
<symbol id=${C.ICON_LINK_SYMBOL_ID} viewBox="0 0 100 100"><path d="M52.8 34.6c.8.8 1.8 1.2 2.8 1.2s2-.4 2.8-1.2c1.5-1.5 1.5-4 0-5.6l-5.2-5.2h26v30.6c0 2.2 1.8 3.9 3.9 3.9 2.2 0 3.9-1.8 3.9-3.9V19.8c0-2.2-1.8-3.9-3.9-3.9h-30l5.2-5.2c1.5-1.5 1.5-4 0-5.6s-4-1.5-5.6 0l-11.8 12c-1.5 1.5-1.5 4 0 5.6l11.9 11.9zM31.1 28.7V11c0-3-2.5-5.5-5.5-5.5H8C5 5.5 2.5 8 2.5 11v17.7c0 3 2.5 5.5 5.5 5.5h17.7c3 0 5.4-2.5 5.4-5.5zM47.2 65.4c-1.5-1.5-4-1.5-5.6 0s-1.5 4 0 5.6l5.2 5.2h-26V45.6c0-2.2-1.8-3.9-3.9-3.9S13 43.5 13 45.6v34.5c0 2.2 1.8 3.9 3.9 3.9h30l-5.2 5.2c-1.5 1.5-1.5 4 0 5.6.8.8 1.8 1.2 2.8 1.2s2-.4 2.8-1.2l11.9-11.9c1.5-1.5 1.5-4 0-5.6l-12-11.9zM92 65.8H74.4c-3 0-5.5 2.5-5.5 5.5V89c0 3 2.5 5.5 5.5 5.5H92c3 0 5.5-2.5 5.5-5.5V71.3c0-3-2.5-5.5-5.5-5.5z"/></symbol>
<symbol id=${C.ICON_SPINNER_SYMBOL_ID} viewBox="0 0 32 32"><path d="M16 32c-4.274 0-8.292-1.664-11.314-4.686s-4.686-7.040-4.686-11.314c0-3.026 0.849-5.973 2.456-8.522 1.563-2.478 3.771-4.48 6.386-5.791l1.344 2.682c-2.126 1.065-3.922 2.693-5.192 4.708-1.305 2.069-1.994 4.462-1.994 6.922 0 7.168 5.832 13 13 13s13-5.832 13-13c0-2.459-0.69-4.853-1.994-6.922-1.271-2.015-3.066-3.643-5.192-4.708l1.344-2.682c2.615 1.31 4.824 3.313 6.386 5.791 1.607 2.549 2.456 5.495 2.456 8.522 0 4.274-1.664 8.292-4.686 11.314s-7.040 4.686-11.314 4.686z"/></symbol>
</defs>`;

	registerAddedNode(C.ICON_SVG_TEMPLATE_ID, `#` + C.ICON_SVG_TEMPLATE_ID);
	document.head.append(template);
}

function ensureIconStylesAdded() {
	if (document.querySelector(`#` + C.ICON_STYLES_ID)) return;

	const style = document.createElement(`style`);
	style.id = C.ICON_STYLES_ID;
	style.innerHTML = `
	.${C.ICON_SPINNER_CLASS} {
		display: none;
		animation: spins 2s linear infinite;
	}
	.${C.ICON_LINK_CLASS} {
		display: inline-block;
	}
	.${C.ICON_LOADING_CLASS} > .${C.ICON_LINK_CLASS} {
		display: none;
	}
	.${C.ICON_LOADING_CLASS} > .${C.ICON_SPINNER_CLASS} {
		display: inline-block;
	}
	.${C.ICON_CLASS} > svg {
		vertical-align: sub;
		height: 1em; width: 1em;
		stroke: currentColor;
		fill: currentColor;
	}`;

	registerAddedNode(C.ICON_STYLES_ID, `#` + C.ICON_STYLES_ID);
	document.head.append(style);
}

