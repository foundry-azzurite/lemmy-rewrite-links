'use strict';

import {trace} from '../../debug.js';
import {stopEventHandler} from '../helpers.js';
import {registerAddedNode} from '../../our-changes.js';
import C from '../../constants.js';
import tippy from '../../tippy.js';
import {getIcon} from './icon.js';

export {
	createOriginalLinkTooltip,
	createLinkLoadTooltip,
	linkLoadTooltipSuccess,
	linkLoadTooltipError
}

function getOriginalLinkHtml(originalHref) {
	registerAddedNode(C.ORIGINAL_LINK_CLASS, `.` + C.ORIGINAL_LINK_CLASS);
	return `Original link: <a class="${C.ORIGINAL_LINK_CLASS}" href="${originalHref}">${originalHref}</a>`;
}

function defaultOptions(link) {
	return {
		appendTo: () => link.parentNode,
		allowHTML: true,
		interactive: true,
		animation: false,
		placement: 'bottom',
		hideOnClick: false
	};
}

function createOriginalLinkTooltip(link, originalHref) {
	trace(`Create original link tooltip`, link, originalHref);

	getIcon(link).addEventListener(`click`, stopEventHandler);
	return createLinkTooltip(link, getOriginalLinkHtml(originalHref));
}

function createLinkTooltip(link, content) {
	return tippy(getIcon(link), {
		...defaultOptions(link),
		content
	});
}

function createLinkLoadTooltip(link) {
	trace(`Create link load tooltip`, link);

	getIcon(link).classList.add(C.ICON_LOADING_CLASS);
	return createLinkTooltip(link, `Loading home URL...<br />Don't want to wait? ${getOriginalLinkHtml(link.href)}`);
}

function linkLoadTooltipSuccess(tooltip, originalHref) {
	linkLoadResult(tooltip, `✔️ Changed link to home instance`, getOriginalLinkHtml(originalHref));
}

function linkLoadTooltipError(tooltip, error) {
	linkLoadResult(tooltip, `❌  ` + error);
}

function linkLoadResult(tooltip, result, finalContent = result) {
	const icon = tooltip.reference;
	icon.classList.remove(C.ICON_LOADING_CLASS);
	icon.addEventListener(`click`, stopEventHandler);
	if (tooltip.state.isVisible) {
		tooltip.setContent(result);
		setTimeout(() => {
			tooltip.hide();
			tooltip.setContent(finalContent);
		}, 2000);
	} else {
		tooltip.setContent(finalContent);
	}
}
