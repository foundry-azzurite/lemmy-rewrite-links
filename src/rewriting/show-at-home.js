import {debug} from '../debug.js';
import C from '../constants.js';
import {
	fetchLocalUrl,
	findLocalUrl,
	isInstantlyRewritable,
	isRemoteKbinUrl,
	isRemoteLemmyUrl,
	isRewritableAfterResolving
} from './url-mapping.js';
import {isV17} from './helpers.js';
import {HOME} from '../home.js';
import {registerAddedNode} from '../our-changes.js';

export {
	addShowAtHomeButton
}

function showAtHomeButtonText() {
	const host = new URL(HOME).hostname;
	return `Show on ${host}`;
}

function createShowAtHomeAnchor(localUrl) {
	const showAtHome = document.createElement(`a`);
	showAtHome.dataset.creationHref = location.href;
	showAtHome.classList.add(C.SHOW_AT_HOME_BUTTON_CLASS);
	showAtHome.innerHTML = showAtHomeButtonText();
	showAtHome.href = localUrl;

	registerAddedNode(C.SHOW_AT_HOME_BUTTON_CLASS, `.` + C.SHOW_AT_HOME_BUTTON_CLASS);

	return showAtHome;
}

function addLemmyShowAtHomeButton(localUrl) {
	const logo = document.querySelector(`a.navbar-brand`);
	const navbarIcons = isV17() ? document.querySelector(`[title="Search"]`)?.closest(`.navbar-nav`) :
		document.querySelector(`#navbarIcons`);
	if (!logo || !navbarIcons) {
		debug(`Could not find position to insert ShowAtHomeButton at`);
		return false;
	}
	const mobile = createShowAtHomeAnchor(localUrl);
	mobile.classList.add(`d-md-none`);
	mobile.style[`margin-right`] = `8px`;
	mobile.style[`margin-left`] = `auto`;
	if (isV17()) {
		document.querySelector(`.navbar-nav.ml-auto`)?.classList.remove(`ml-auto`);
	}

	logo.insertAdjacentElement('afterend', mobile);

	const desktop = createShowAtHomeAnchor(localUrl);
	desktop.classList.add(`d-md-inline`);
	desktop.style[`margin-right`] = `8px`;
	navbarIcons.insertAdjacentElement('beforebegin', desktop);
	return true;
}

function addKbinShowAtHomeButton(localUrl) {
	const prependTo = document.querySelector(`#header menu:not(.head-nav__menu)`)
	if (!prependTo) {
		debug(`Could not find position to insert ShowAtHomeButton at`);
		return false;
	}

	const item = document.createElement(`li`);
	item.append(createShowAtHomeAnchor(localUrl));
	prependTo.prepend(item);
	return true;
}

function addButton(localUrl) {
	const oldButton = document.querySelectorAll(`.` + C.SHOW_AT_HOME_BUTTON_CLASS);
	if (oldButton.length > 0 && oldButton[0].dataset.creationHref !== location.href) {
		debug(`Removing old show at home button`);
		oldButton.forEach(btn => btn.remove()); // for SPA (like Lemmy) we need to watch when the location changes and update
	} else if (oldButton.length > 0) {
		debug(`Old show at home button still exists`);
		return false;
	}

	if (!localUrl) {
		debug(`No local URL for show at home button found`);
		return false;
	} else if (isRemoteLemmyUrl(location)) {
		return addLemmyShowAtHomeButton(localUrl);
	} else if (isRemoteKbinUrl(location)) {
		return addKbinShowAtHomeButton(localUrl);
	} else {
		return false;
	}
}

async function addShowAtHomeButton() {
	if (isInstantlyRewritable(location)) {
		return addButton(findLocalUrl(location));
	} else if (isRewritableAfterResolving(location)) {
		try {
			return addButton(await fetchLocalUrl(location));
		} catch (e) {
			debug(`Error while trying to add "show at home" button`, e);
		}
	}
}
