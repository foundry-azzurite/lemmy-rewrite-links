import {debug, trace} from "../debug.js";
import {HOME} from "../home.js";
import {getAddedNodesSelectors} from "../our-changes.js";
import {rewriteLinksToLocal} from './links/links.js';
import {isRemoteUrl} from './url-mapping.js';
import {addSettingsButton} from './settings-buttons.js';
import {addShowAtHomeButton} from './show-at-home.js';

export {
	startRewriting,
	triggerRewrite
}

function triggerRewrite() {
	doAllDomChanges();
}

function isOrHasOurAddedNode(node) {
	return getAddedNodesSelectors().some(selector => node.matches?.(selector) || node.querySelector?.(selector));
}

function isIrrelevantChange(change) {
	if (change.type === `childList`) {
		if (Array.from(change.removedNodes).some(isOrHasOurAddedNode)) {
			trace(`Change`, change, `is relevant because a removed node is/has ours`);
			return false;
		}
		if (!Array.from(change.addedNodes).every(isOrHasOurAddedNode)) {
			trace(`Change`, change, `is relevant because not every added node is/has ours`);
			return false;
		}
	} else if (change.type === `attributes` && isRemoteUrl(new URL(change.target.href, location.origin))) {
		trace(`Change`, change, `is relevant because href changed to a remote URL`);
		return false;
	}

	trace(`Change`, change, `is irrelevant`);
	return true;
}

async function startRewriting() {
	new MutationObserver((changes, observer) => {
		debug(`MutationObserver triggered`, changes);
		if (changes.every(isIrrelevantChange)) {
			debug(`All observed changes are irrelevant`);
			return;
		}
		doAllDomChanges(changes);
	}).observe(document.body, {
		subtree: true,
		childList: true,
		attributeFilter: [`href`]
	});

	await doAllDomChanges();
}

async function doAllDomChanges(changes) {
	debug(`doAllDomChanges start`);

	const addedSettingsButtons = addSettingsButton();
	if (addedSettingsButtons) debug(`Added Settings Buttons`);

	const addedShowAtHomeButton = HOME ? addShowAtHomeButton() : false;
	if (addedShowAtHomeButton) debug(`Added Show At Home Button`);

	const rewrittenLinks = HOME ? await rewriteLinksToLocal(changes) : false;
	if (rewrittenLinks) debug(`Rewritten some links`);

	debug(`doAllDomChanges end`);
}
