import {HOME, isHomeInstance} from '../home.js';
import {isKbinInstance, isLemmyInstance} from '../instances.js';
import {getAuth} from './auth.js';
import {debug, trace} from '../debug.js';
import {isSamePage} from './helpers.js';
import C from '../constants.js';
import {performXmlHttpRequest} from '../gm.js';

export {
	findLocalUrl,
	fetchLocalUrl,
	isRemoteUrl,
	isRemoteLemmyUrl,
	isRemoteKbinUrl,
	isInstantlyRewritable,
	isRewritableAfterResolving,
	clearMissingUrlsInCache
}

function splitPaths(url) {
	return url.pathname.split(`/`).slice(1);
}

function isRemoteLemmyUrl(url) {
	return !isHomeInstance(url) && isLemmyInstance(url);
}

function isRemoteKbinUrl(url) {
	return !isHomeInstance(url) && isKbinInstance(url);
}

function isRemoteUrl(url) {
	return isRemoteLemmyUrl(url) || isRemoteKbinUrl(url);
}

/**
 * @param url remote url to find local url for
 * @param rootPath optional, otherwise uses root path of {@link url}
 * @returns {string}
 */
function findLocalUrlForStandardAtFormat(url, rootPath) {
	const paths = splitPaths(url);

	const name = paths[1].includes(`@`) ? paths[1] : paths[1] + `@` + url.host;
	return `${HOME}/${rootPath || paths[0]}/${name}` + url.search + url.hash;
}

function findLocalUrlForLemmyUrl(url) {
	if (isLemmyUserOrCommunityUrl(url)) {
		return findLocalUrlForStandardAtFormat(url);
	} else {
		return null;
	}
}

function findLocalUrlForKbinUserUrl(url) {
	const paths = splitPaths(url);

	const user = paths[1].startsWith(`@`) ? paths[1].substring(1) : paths[1];
	const name = user.includes(`@`) ? user : user + `@` + url.host;
	return `${HOME}/u/${name}` + url.search + url.hash;
}

function findLocalUrlForKbinUrl(url) {
	if (isKbinMagazineUrl(url)) {
		return findLocalUrlForStandardAtFormat(url, mappedKbinRootPath(url));
	} else if (isKbinUserUrl(url)) {
		return findLocalUrlForKbinUserUrl(url);
	} else {
		return null;
	}
}

function findLocalUrl(url) {
	if (isRemoteLemmyUrl(url)) return findLocalUrlForLemmyUrl(url);
	if (isRemoteKbinUrl(url)) return findLocalUrlForKbinUrl(url);
	return null;
}

async function fetchApIdFromRemote(url) {
	const endpoint = isLemmyPostUrl(url) ? `post` : `comment`;
	const paths = splitPaths(url);
	const id = paths[1];
	return new Promise((resolve, reject) => {
		performXmlHttpRequest(`${url.origin}/api/v3/${endpoint}?id=${id}`, (response, data) => {
			const apId = data[`${endpoint}_view`]?.[endpoint]?.ap_id;
			if (response.status === 200 && apId) {
				resolve(apId);
			} else {
				handleFailedRequest(`fetching AP ID`, response, reject);
			}
		});
	});
}

function handleFailedRequest(requestName, response, reject) {
	if (response.status >= 200 && response.status <= 299) {
		reject(`${requestName}: Unhandled successful response, status: ${response.status}`);
	} else if (response.status >= 400 && response.status <= 599) {
		reject(`${requestName}: Error, status: ${response.status}`);
	} else {
		reject(`${requestName}: Something weird happened, status: ${response.status}`)
	}
}

async function resolveObjectFromHome(url) {
	return new Promise(async (resolve, reject) => {

		const auth = await getAuth();
		if (!auth) {
			debug(`No auth token found`);
			reject(C.AUTH_MISSING);
		}
			
		performXmlHttpRequest(`${HOME}/api/v3/resolve_object?auth=${auth}&q=${encodeURIComponent(url.href)}`,
			(response, data) => {
				if (response.status === 200 && data.post?.post?.id) {
					resolve(`${HOME}/post/${data.post.post.id}${url.search}${url.hash}`);
				} else if (response.status === 200 && data.comment?.comment?.id) {
					resolve(`${HOME}/comment/${data.comment.comment.id}${url.search}${url.hash}`);
				} else if (response.status === 400 && data?.error === `couldnt_find_object`) {
					resolve(null);
				} else if (response.status === 400 && data?.error === `not_logged_in`) {
					reject(C.AUTH_WRONG);
				} else {
					handleFailedRequest(`resolving object`, response, reject);
				}
			});
	});
}

const urlCache = {};

function clearMissingUrlsInCache() {
	for (const value of Object.values(urlCache)) {
		if (value.error) delete value.error;
		if (value.localUrl === null) delete value.localUrl;
	}
}

function getCacheKey(url) {
	return url.host + url.pathname + url.search;
}

function cacheResult(url, localUrl) {
	const key = getCacheKey(url);
	if (!urlCache[key]) {
		urlCache[key] = {};
	}
	if (urlCache[key].error) delete urlCache[key].error; 
	urlCache[key].localUrl = localUrl;
	return localUrl;
}

function cacheErrorResult(url, error) {
	const key = getCacheKey(url);
	if (!urlCache[key]) {
		urlCache[key] = {};
	}
	urlCache[key].error = error;
}

function getLocalUrlfromCache(url) {
	const key = getCacheKey(url);
	if (urlCache[key]?.error) {
		throw urlCache[key]?.error;
	} else {
		return urlCache[key]?.localUrl;
	}
}

async function fetchLocalUrl(url, loadFromCache = true) {
	if (loadFromCache) {
		const cached = getLocalUrlfromCache(url);
		if (cached !== undefined) {
			trace(`Found URL ${url} in cache: ${cached}`);
			return cached;
		}
	}
	try {
		return cacheResult(url, await fetchLocalUrlNoCache(url));
	} catch (e) {
		debug(`fetchLocalUrl error`, e);
		cacheErrorResult(url, e);
		throw e;
	}
}

async function fetchLocalUrlNoCache(url) {
	trace(`Trying to resolve URL ${url} directly`);
	const localUrl = await resolveObjectFromHome(url);
	if (localUrl !== null) {
		return localUrl;
	} else {
		trace(`Did not find URL ${url} directly`);
	}

	// if localUrl is null then we're likely trying to resolve a remote-local URL, so we need to get the
	// ActivityPub ID first
	const apId = new URL(await fetchApIdFromRemote(url));
	trace(`Found AP ID for URL ${url}: ${apId}`);
	if (!apId.search) {
		apId.search = url.search;
	}
	if (!apId.hash) {
		apId.hash = url.hash;
	}
	if (isSamePage(url, apId)) {
		trace(`Previous URL was AP ID already, URL not federated for some reason`);
		return null;
	} else {
		return await resolveObjectFromHome(apId);
	}
}

function isInstantlyRewritable(url) {
	return isRemoteLemmyUrl(url) && isLemmyUserOrCommunityUrl(url)
		|| isRemoteKbinUrl(url) && (isKbinMagazineUrl(url) || isKbinUserUrl(url));
}

function isRewritableAfterResolving(url) {
	return isRemoteLemmyUrl(url) && (isLemmyPostUrl(url) || isLemmyCommentUrl(url));
}

function isLemmyPostUrl(url) {
	const paths = splitPaths(url);
	return paths[0] === `post`;
}

function isLemmyCommentUrl(url) {
	const paths = splitPaths(url);
	return paths[0] === `comment`;
}

function isLemmyUserOrCommunityUrl(url) {
	const paths = splitPaths(url);
	return paths[0] === `c` || paths[0] === `u`;
}

function isKbinPostUrl(url) {
	const paths = splitPaths(url);
	return paths[0] === `m` && paths.length > 2 && paths[2] === `t`;
}

function isKbinMicroblogUrl(url) {
	const paths = splitPaths(url);
	return paths[0] === `m` && paths.length > 2 && paths[2] === `p`;
}

function isKbinMicroblogOverviewUrl(url) {
	const paths = splitPaths(url);
	return paths[0] === `m` && paths.length > 2 && paths[2] === `microblog`;
}

function isKbinMagazinePeopleUrl(url) {
	const paths = splitPaths(url);
	return paths[0] === `m` && paths.length > 2 && paths[2] === `people`;
}

function isKbinMagazineUrl(url) {
	const paths = splitPaths(url);
	return paths[0] === `m`
		&& !isKbinPostUrl(url)
		&& !isKbinMagazinePeopleUrl(url)
		&& !isKbinMicroblogUrl(url)
		&& !isKbinMicroblogOverviewUrl(url);
}

function mappedKbinRootPath(url) {
	const paths = splitPaths(url);
	if (paths[0] === `m`) {
		return `c`
	} else {
		return null;
	}
}

function isKbinUserUrl(url) {
	const paths = splitPaths(url);
	return paths.length === 2 && paths[0] === `u`;
}

