import {HOME} from '../home.js';
import {debug} from '../debug.js';
import {triggerRewrite} from './rewrite.js';
import {clearMissingUrlsInCache} from './url-mapping.js';
import C from '../constants.js';
import * as GM from "../gm.js";

export {
	initAuth,
	getAuth,
	updateAuthPeriodically
}

let AUTH;

function getAuthFromCookie() {
	return document.cookie
		.split("; ")
		.find((row) => row.startsWith("jwt="))
		?.split("=")[1];
}

async function setAuth(auth) {
	AUTH = auth;
	await GM.setValue(`auth`, auth);
}

async function initAuth() {
	const curAuth = await getAuth();
	if (curAuth) {
		AUTH = curAuth;
		return;
	}
	if (location.origin === HOME) {
		const newAuth = getAuthFromCookie();
		await setAuth(newAuth);

		if (newAuth && await GM.getValue(`authNoticeShown`)) {
			alert(`Lemmy Universal Link Switcher: Post/comment rewriting has been set up successfully`);
			await GM.setValue(`authNoticeShown`, null);
		}
	} else if (HOME && !(await GM.getValue(`authNoticeShown`))) {
		await GM.setValue(`authNoticeShown`, `true`);
		alert(C.SETUP_AUTH_MESSAGE);
	}
}

function updateAuthPeriodically() {
	setInterval(async () => {
		const prev = AUTH;
		
		const newAuth = location.origin === HOME ? getAuthFromCookie() : await getAuth();

		if (prev !== newAuth) {
			debug(`Auth changed`);
			await setAuth(newAuth);
			clearMissingUrlsInCache();
			triggerRewrite();
		}
	}, 1234);
}


async function getAuth() {
	return await GM.getValue(`auth`);
}
