import {setHome} from '../home.js';
import {initGMValueMocking} from '../gm.test.js';

export function setTestHome(home) {
	initGMValueMocking();
	setHome(home);
}
