export function isHashLink(link) {
	return link.hash && link.origin + link.pathname + link.search === location.origin + location.pathname +
		location.search;
}

export function isSamePage(url1, url2) {
	return url1.host === url2.host && url1.pathname === url2.pathname;
}

export function isV17() {
	return window.isoData?.site_res?.version.startsWith(`0.17`);
}

export const stopEventHandler = (event) => {
	// for mobile so the tooltip can be opened
	event.preventDefault();
	event.stopPropagation();
};
