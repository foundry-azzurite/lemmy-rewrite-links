import C from '../constants.js';
import {registerAddedNode} from '../our-changes.js';
import {showSettings} from '../settings.js';

export {
	addSettingsButton
}

function addSettingsButton() {
	if (location.pathname !== `/settings`) return false;
	if (!document.querySelector(`[name="Description"][content="Lemmy"]`)) return false;
	if (document.querySelector(`#` + C.SETTINGS_BUTTON_ID)) return false;
	
	const insertAfter = document.querySelector(`#user-password`)?.closest(`.card`);
	if (!insertAfter) return;

	const button = document.createElement(`button`);
	button.id = C.SETTINGS_BUTTON_ID;

	button.setAttribute(`class`, `btn btn-block btn-primary mr-4 w-100`)
	button.innerHTML = `Lemmy Universal Link Switcher Settings`;
	button.addEventListener(`click`, showSettings);
	
	registerAddedNode(C.SETTINGS_BUTTON_ID, `#` + C.SETTINGS_BUTTON_ID);
	insertAfter.insertAdjacentElement('afterend', button);

	return true;
}

