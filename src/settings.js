import {registerMenuCommand} from './gm.js';
import C from './constants.js';
import {getSecondaryHomeInstances, HOME, setHome, setSecondaryHomeInstances} from './home.js';
import tippy from './tippy.js';
import {registerAddedNode} from './our-changes.js';

export {
	initSettings,
	showSettings
}

let refreshMethods = [];

async function refresh() {
	for (const refreshMethod of refreshMethods) {
		await refreshMethod();
	}
}

function closeSettings() {
	document.querySelector(`#` + C.SETTINGS_MENU_ID)?.remove();
	refreshMethods = [];
}

function initSettings() {
	registerMenuCommand(`Open Settings`, showSettings);
}

function styles(elem, style) {
	for (const [prop, val] of Object.entries(style)) {
		elem.style[prop] = val;
	}
}

const defaultBorder = `1px solid #AAA`;

/**
 *
 * @param content
 * @param {{inline: boolean}} options
 * @returns {any}
 */
function createButton(content, options = {}) {
	const button = document.createElement(`div`);
	button.innerHTML = content;
	styles(button, {
		display: options.inline ? `inline-block` : `block`,
		backgroundColor: `#666`,
		textAlign: `center`,
		border: defaultBorder,
		cursor: `pointer`,
		borderRadius: `8px 8px`,
		padding: `5px`
	})
	return button;
}

function ensureMenuStylesAdded() {
	if (document.querySelector(`#` + C.SETTINGS_STYLES_ID)) return;
	const style = document.createElement(`style`);
	style.id = C.SETTINGS_STYLES_ID;
	style.innerHTML = `
	#${C.SETTINGS_MENU_ID} * {
		box-sizing: border-box;
	}
	`;
	document.head.append(style);
}

function showSettings() {
	if (document.querySelector(`#` + C.SETTINGS_MENU_ID)) return;
	if (window !== window.top) return;

	ensureMenuStylesAdded();

	const background = document.createElement(`div`);
	background.id = C.SETTINGS_MENU_ID;
	registerAddedNode(C.SETTINGS_MENU_ID, `#` + C.SETTINGS_MENU_ID);
	styles(background, {
		position: `fixed`,
		top: `0`,
		left: `0`,
		zIndex: 9999,
		width: `100vw`,
		height: `100vh`,
		backgroundColor: `#00000099`,
		backdropFilter: `blur(6px)`,
		display: `grid`,
		grid: `grid`,
		alignItems: `center`,
		justifyContent: `space-around`,
	});
	background.addEventListener(`click`, (event) => {
		if (event.target === background) {
			closeSettings();
		}
	});

	const menu = document.createElement(`div`);
	styles(menu, {
		position: `relative`,
		color: `#ddd`,
		maxWidth: `90vw`,
		maxHeight: `90vh`,
		backgroundColor: `#555`,
		padding: `20px`,
		border: defaultBorder,
		borderRadius: `8px`,
		overflow: `hidden auto`,
	});
	background.append(menu);

	const closeButton = createButton(`🗙`);
	styles(closeButton, {
		position: `absolute`,
		top: `-1px`,
		right: `-1px`,
		width: `30px`,
		height: `30px`,
		lineHeight: `25px`,
		fontSize: `25px`,
		borderRadius: `0 8px`,
		padding: 0
	});
	closeButton.addEventListener(`click`, () => closeSettings());
	menu.append(closeButton);

	const menuHeader = createSettingHeader(`Lemmy Universal Link Switcher Settings`, 1.5)
	styles(menuHeader, {
		textDecoration: `underline`,
		marginTop: 0
	});
	menu.append(menuHeader);

	addMainHomeInstanceSetting(menu);
	addMoreHomeInstancesSettings(menu);

	document.body.append(background);
}

function createSettingHeader(text, sizeMultiplicator = 1) {
	const header = document.createElement(`div`);
	header.innerHTML = text;
	styles(header, {
		marginRight: `10px`,
		fontSize: `${sizeMultiplicator * 1.4}em`,
		lineHeight: `${sizeMultiplicator * 1.4}em`,
		fontWeight: `bold`,
		marginTop: `${sizeMultiplicator * 25}px`,
		marginBottom: `${sizeMultiplicator * 10}px`
	});
	return header;
}

function addInfo(elem, content) {
	const info = document.createElement(`span`);
	info.innerHTML = ` 🛈`;
	tippy(info, {
		content,
		placement: 'bottom',
		triggerTarget: elem
	});
	elem.append(info);
}

/**
 *
 * @param {{getter: function, setter: function, validator: function, placeholder: string}} options
 * @returns {HTMLInputElement}
 */
function createInput(options) {
	const inputElement = document.createElement(`input`);
	styles(inputElement, {
		width: `100%`,
		margin: 0
	});
	if (options.placeholder) {
		inputElement.placeholder = options.placeholder;
	}
	if (options.getter) {
		Promise.resolve(options.getter())
			.then(result => inputElement.value = result);
	}
	if (!inputElement.value) {
		inputElement.setCustomValidity(`empty`);
	}
	styles(inputElement, {border: defaultBorder});

	if (!options.validator && !options.setter) {
		return;
	}
	let validator = options.validator || (() => true);
	let setter = options.setter || (() => {});
	inputElement.addEventListener(`input`, async () => {
		const validated = await validator(inputElement.value);
		if (!inputElement.value || validated) {
			inputElement.setCustomValidity(`empty`);
			styles(inputElement, {
				border: defaultBorder
			});
		}
		if (inputElement.value) {
			if (validated) {
				inputElement.setCustomValidity(``);
				await setter(inputElement.value);
			} else if (!validated) {
				inputElement.setCustomValidity(`fail`);
				styles(inputElement, {
					border: `1px solid red`
				});
			}
		}
	});
	return inputElement;
}

const urlValidator = (value) => {
	try {
		new URL(value);
		return true;
	} catch (e) {
		return false;
	}
};

const instancePlaceholder = `The full link (including http(s)://) to the instance`;

function addMainHomeInstanceSetting(addTo) {
	const header = createSettingHeader(`Main Home Instance`);
	addInfo(header, `All links will be rewritten to this instance, except for links to your secondary home instances.`);
	addTo.append(header);

	const mainInstance = createInput({
		getter: () => HOME,
		setter: (value) => {
			const url = new URL(value);
			if (HOME !== url.origin) {
				setHome(url.origin);
				refresh();
			}
		},
		validator: urlValidator,
		placeholder: instancePlaceholder
	});
	addTo.append(mainInstance);

	const homeButtonWrapper = document.createElement(`div`);
	const refreshMakeHomeButton = () => {
		homeButtonWrapper.replaceChildren();

		if (location.origin === HOME) return;

		const makeHomeButton = createButton(`Use current page as home instance`);
		styles(makeHomeButton, {
			borderRadius: `0 0 8px 8px`
		});
		makeHomeButton.addEventListener(`click`, () => {
			mainInstance.value = location.origin;
			mainInstance.dispatchEvent(new Event(`input`));
		});
		homeButtonWrapper.append(makeHomeButton);
	}
	refreshMakeHomeButton();
	refreshMethods.push(refreshMakeHomeButton);
	addTo.append(homeButtonWrapper);
}

async function addMoreHomeInstancesSettings(addTo) {
	const header = createSettingHeader(`Secondary Home Instances`);
	addInfo(header, `All links pointing to these instances will not be changed.`);
	addTo.append(header);

	addTo.append(createListInput(
		async () => await getSecondaryHomeInstances(),
		async (value) => {
			const homeInstances = await getSecondaryHomeInstances();
			homeInstances.push(new URL(value).origin);
			await setSecondaryHomeInstances(homeInstances);
		},
		async (value) => {
			const homeInstances = await getSecondaryHomeInstances();
			var index = homeInstances.indexOf(new URL(value).origin);
			if (index > -1) {
				homeInstances.splice(index, 1);
			}
			await setSecondaryHomeInstances(homeInstances);
		}
	));
}

function createListItem(item, onClick) {
	const listItem = createButton(item + ` 🗙`, {inline: true});
	styles(listItem, {
		margin: `0px 5px 5px 0`
	});
	listItem.addEventListener(`click`, onClick);
	return listItem;
}

function createListInput(getter, add, remove) {
	const wrapper = document.createElement(`div`);

	const list = document.createElement(`div`);
	styles(list, {
		marginBottom: `8px`
	});
	const refreshList = async () => {
		const items = (await getter())
			.sort()
			.map((item) => createListItem(item, async () => {
				await remove(item);
				refresh();
			}));
		if (items.length) {
			list.replaceChildren(...items);
		} else {
			list.replaceChildren(`<None>`);
		}
	}
	refreshList();
	refreshMethods.push(refreshList);
	wrapper.append(list);

	const addInput = createInput({
		validator: async (value) => {
			return urlValidator(value) && !(await getter()).includes(value);
		},
		placeholder: instancePlaceholder
	});
	wrapper.append(addInput);

	const buttonWrapper = document.createElement(`div`);
	const refreshButtons = async () => {
		buttonWrapper.replaceChildren();

		const isCurrentPageHome = HOME === location.origin || (await getter()).includes(location.origin);

		const addButton = createButton(`Add`);
		styles(addButton, {
			borderRadius: isCurrentPageHome ? `0 0 8px 8px` : `0`
		})
		addButton.addEventListener(`click`, async () => {
			if (addInput.validity.valid) {
				await add(addInput.value);
				addInput.value = ``;
				refresh();
			}
		})
		buttonWrapper.append(addButton);

		if (!isCurrentPageHome) {
			const addCurrentButton = createButton(`Add current page`);
			styles(addCurrentButton, {
				borderRadius: `0 0 8px 8px`
			})
			addCurrentButton.addEventListener(`click`, async () => {
				await add(location.origin);
				addInput.value = ``;
				refresh();
			});
			buttonWrapper.append(addCurrentButton);
		}
	}
	refreshButtons();
	refreshMethods.push(refreshButtons);
	wrapper.append(buttonWrapper);

	return wrapper;
}
