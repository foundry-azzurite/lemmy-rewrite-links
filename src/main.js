import {initHome, updateHomePeriodically} from "./home.js";
import {startRewriting} from "./rewriting/rewrite.js";
import {initAuth, updateAuthPeriodically} from './rewriting/auth.js';
import {initSettings} from './settings.js';

(async () => {
	await initHome();
	updateHomePeriodically();
	await initAuth();
	updateAuthPeriodically();
	
	initSettings();
	
	startRewriting();
})();
