import {isLemmyInstance} from "./instances.js";
import {triggerRewrite} from "./rewriting/rewrite.js";
import {debug, trace} from "./debug.js";
import * as GM from "./gm.js";

export let HOME; // TODO replace with getHome()
let secondaryHomes = []; // TODO remove this then

export async function initHome() {
	HOME = await GM.getValue(`home`);
	secondaryHomes = await getSecondaryHomeInstances();
	if (!HOME && isLemmyInstance(location) &&
		confirm(`Lemmy Universal Link Switcher: Set this instance to be your home instance to which all URLs get rewritten to?`)) {

		setHome(location.origin);
	}
}

export async function setHome(newHome) {
	trace(`Set HOME from ${HOME} to ${newHome}`);
	if (typeof newHome !== `string`) {
		newHome = null;
	}
	HOME = newHome;
	await GM.setValue(`home`, newHome);
}

export async function getHome() {
	return await GM.getValue(`home`);
}

export function updateHomePeriodically() {
	trace(`Current HOME`, HOME);
	setInterval(async () => {
		const prevHome = HOME;
		const prevSecondaries = secondaryHomes;

		HOME = await getHome();
		secondaryHomes = await getSecondaryHomeInstances();

		if (prevHome !== HOME) {
			debug(`HOME changed externally from`, prevHome, `to`, HOME);
			triggerRewrite();
		} else if (prevSecondaries.length !== secondaryHomes.length || !prevSecondaries.every((v) => secondaryHomes.includes(v))) {
			debug(`secondaryHomes changed externally from`, prevSecondaries, `to`, secondaryHomes);
			triggerRewrite();
		}
	}, 1337)
}

export function isHomeInstance(url) {
	return secondaryHomes
		.concat(HOME)
		.includes(url.origin);
}

export async function getSecondaryHomeInstances() {
	const homeInstancesStr = await GM.getValue(`secondaryHomes`);
	return homeInstancesStr ? JSON.parse(homeInstancesStr) : [];
}

export async function setSecondaryHomeInstances(homeInstances) {
	await GM.setValue(`secondaryHomes`, JSON.stringify(homeInstances));
}
