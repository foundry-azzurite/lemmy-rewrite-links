import {debug, trace} from './debug.js';

export async function setValue(key, value) {
	trace(`GM.setValue key ${key}, value ${value}`)
	await GM.setValue(key, value);
}

export async function getValue(key) {
	return await GM.getValue(key);
}

function parseResponse(response) {
	try {
		return JSON.parse(response.response);
	} catch (e) {
		debug(`Error parsing response JSON`, e);
		return response.response;
	}
}

function logRequest(response, data) {
	trace(`FinalUrl`, response.finalUrl, 
		`status`, response.status, 
		`text`, response.statusText, 
		`response`,	data || response.response);
	trace(`responseHeaders`, response.responseHeaders);
}

export function performXmlHttpRequest(url, doneCallback) {
	GM.xmlHttpRequest({
		url,
		onloadend: (response) => {
			const data = parseResponse(response);
			logRequest(response, data);
			doneCallback(response, data);
		}
	});
}

export function registerMenuCommand(name, onClick) {
	GM.registerMenuCommand(name, onClick);
}
