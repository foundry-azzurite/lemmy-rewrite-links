'use strict';


const values = {};

export function mockGMValue(key, value) {
	initGMValueMocking();
	window.GM.setValue(key, value);
}

export function initGMValueMocking() {
	if (!window.GM) {
		window.GM = {};
	}
	if (!window.GM.setValue) {
		window.GM.setValue = async (key, value) => {
			values[key] = `` + value;
		} 
	}
	if (!window.GM.getValue) {
		window.GM.getValue = async (key) => {
			return values[key];
		} 
	}
}
