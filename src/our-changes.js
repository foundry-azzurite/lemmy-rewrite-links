const OUR_CHANGES = {addedNodes: {}};

export function getAddedNodesSelectors() {
	return Object.values(OUR_CHANGES.addedNodes);
}

export function registerAddedNode(id, selector) {
	OUR_CHANGES.addedNodes[id] = selector;
}
