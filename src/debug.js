let DEBUG = false;

export function debug() {
	if (DEBUG) console.debug(`Rewriter | `, ...arguments);
}

export function trace() {
	if (DEBUG === `trace`) console.debug(`Rewriter Trace | `, ...arguments);
}
